const path = require("path");
const OpenAPI = require("openapi-typescript-codegen");

const { PWD } = process.env;

const specPath = path.resolve(PWD, "../api/swagger-spec.json");
const distPath = path.resolve(PWD, "src/api/generate");

(async () => {
  await OpenAPI.generate({
    input: require(specPath),
    output: distPath,
  });
})();
