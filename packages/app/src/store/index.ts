import { configureStore } from "@reduxjs/toolkit";
import createSagaMiddleware from "redux-saga";
import { tableSlice } from "./table";
import { rootTableSaga } from "./table/saga";

let sagaMiddleware = createSagaMiddleware();

export const store = configureStore({
  reducer: { table: tableSlice.reducer },
  middleware: (getDefaultMiddleware) => [
    sagaMiddleware,
    ...getDefaultMiddleware(),
  ],
});

sagaMiddleware.run(rootTableSaga);

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
