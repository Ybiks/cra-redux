import "react-redux";

import { RootState } from "./index";

// and extend them!
declare module "react-redux" {
  // eslint-disable-next-line @typescript-eslint/no-empty-interface
  export interface DefaultRootState extends RootState {}
}
