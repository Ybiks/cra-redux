import { TableTypes } from "../../../api";

export const tableActions = {
  FETCH_TABLE_DATA: "FETCH_TABLE_DATA",
} as const;

export type FetchTableDataAction = {
  type: typeof tableActions.FETCH_TABLE_DATA;
  payload: { table: TableTypes };
};
