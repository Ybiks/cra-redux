import { call, put } from "redux-saga/effects";
import { setTableData } from "..";
import { matchingMethods, TableFields } from "../../../api";
import { FetchTableDataAction } from "./actions";

export function* fetchTableData(action: FetchTableDataAction) {
  try {
    const data: TableFields<FetchTableDataAction["payload"]["table"]>[] =
      yield call(matchingMethods[action.payload.table].getAll);

    yield put(setTableData({ key: action.payload.table, data, params: {} }));
  } catch (err) {
    console.log(err);
  }
}
