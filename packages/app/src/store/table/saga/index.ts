import { takeEvery } from "redux-saga/effects";

import { tableActions } from "./actions";
import { fetchTableData } from "./fetch-table-data";

export function* rootTableSaga() {
  yield takeEvery(tableActions.FETCH_TABLE_DATA, fetchTableData);
}
