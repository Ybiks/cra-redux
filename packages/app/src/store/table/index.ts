import { createSlice, PayloadAction } from "@reduxjs/toolkit";

import { TableFields, TableTypes } from "../../api";

export type SortParams = { key: string; type: "asc" | "desc" };

export type TableState<T extends TableTypes> = {
  key?: T;
  params: {
    sort?: SortParams;
    take?: number;
  };
  data: TableFields<T>[];
};

const initialState: TableState<TableTypes> = { params: {}, data: [] };

export const tableSlice = createSlice({
  name: "table",
  initialState,
  reducers: {
    setTableData: (_, { payload }: PayloadAction<TableState<TableTypes>>) =>
      payload,
    applySort: (state, { payload }: PayloadAction<SortParams>) => {
      state.params.sort = payload;
    },
  },
});

export const { setTableData, applySort } = tableSlice.actions;
