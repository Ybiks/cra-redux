/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { ClientEntity } from '../models/ClientEntity';
import type { CreateClientDTO } from '../models/CreateClientDTO';
import type { CreateManyClientEntityDto } from '../models/CreateManyClientEntityDto';
import type { GetManyClientEntityResponseDto } from '../models/GetManyClientEntityResponseDto';
import type { UpdateClientDTO } from '../models/UpdateClientDTO';
import { request as __request } from '../core/request';

export class ClientService {

    /**
     * Retrieve a single ClientEntity
     * @param id
     * @param fields Selects resource fields. <a href="https://github.com/nestjsx/crud/wiki/Requests#select" target="_blank">Docs</a>
     * @param join Adds relational resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#join" target="_blank">Docs</a>
     * @param cache Reset cache (if was enabled). <a href="https://github.com/nestjsx/crud/wiki/Requests#cache" target="_blank">Docs</a>
     * @returns ClientEntity Get one base response
     * @throws ApiError
     */
    public static async getOneBaseClientControllerClientEntity(
        id: number,
        fields?: Array<string>,
        join?: Array<string>,
        cache?: number,
    ): Promise<ClientEntity> {
        const result = await __request({
            method: 'GET',
            path: `/clients/${id}`,
            query: {
                'fields': fields,
                'join': join,
                'cache': cache,
            },
        });
        return result.body;
    }

    /**
     * Update a single ClientEntity
     * @param id
     * @param requestBody
     * @returns ClientEntity Response
     * @throws ApiError
     */
    public static async updateOneBaseClientControllerClientEntity(
        id: number,
        requestBody: UpdateClientDTO,
    ): Promise<ClientEntity> {
        const result = await __request({
            method: 'PATCH',
            path: `/clients/${id}`,
            body: requestBody,
        });
        return result.body;
    }

    /**
     * Replace a single ClientEntity
     * @param id
     * @param requestBody
     * @returns ClientEntity Response
     * @throws ApiError
     */
    public static async replaceOneBaseClientControllerClientEntity(
        id: number,
        requestBody: ClientEntity,
    ): Promise<ClientEntity> {
        const result = await __request({
            method: 'PUT',
            path: `/clients/${id}`,
            body: requestBody,
        });
        return result.body;
    }

    /**
     * Delete a single ClientEntity
     * @param id
     * @returns any Delete one base response
     * @throws ApiError
     */
    public static async deleteOneBaseClientControllerClientEntity(
        id: number,
    ): Promise<any> {
        const result = await __request({
            method: 'DELETE',
            path: `/clients/${id}`,
        });
        return result.body;
    }

    /**
     * Retrieve multiple ClientEntities
     * @param fields Selects resource fields. <a href="https://github.com/nestjsx/crud/wiki/Requests#select" target="_blank">Docs</a>
     * @param s Adds search condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#search" target="_blank">Docs</a>
     * @param filter Adds filter condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#filter" target="_blank">Docs</a>
     * @param or Adds OR condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#or" target="_blank">Docs</a>
     * @param sort Adds sort by field. <a href="https://github.com/nestjsx/crud/wiki/Requests#sort" target="_blank">Docs</a>
     * @param join Adds relational resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#join" target="_blank">Docs</a>
     * @param limit Limit amount of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#limit" target="_blank">Docs</a>
     * @param offset Offset amount of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#offset" target="_blank">Docs</a>
     * @param page Page portion of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#page" target="_blank">Docs</a>
     * @param cache Reset cache (if was enabled). <a href="https://github.com/nestjsx/crud/wiki/Requests#cache" target="_blank">Docs</a>
     * @returns any Get many base response
     * @throws ApiError
     */
    public static async getManyBaseClientControllerClientEntity(
        fields?: Array<string>,
        s?: string,
        filter?: Array<string>,
        or?: Array<string>,
        sort?: Array<string>,
        join?: Array<string>,
        limit?: number,
        offset?: number,
        page?: number,
        cache?: number,
    ): Promise<(GetManyClientEntityResponseDto | Array<ClientEntity>)> {
        const result = await __request({
            method: 'GET',
            path: `/clients`,
            query: {
                'fields': fields,
                's': s,
                'filter': filter,
                'or': or,
                'sort': sort,
                'join': join,
                'limit': limit,
                'offset': offset,
                'page': page,
                'cache': cache,
            },
        });
        return result.body;
    }

    /**
     * Create a single ClientEntity
     * @param requestBody
     * @returns ClientEntity Get create one base response
     * @throws ApiError
     */
    public static async createOneBaseClientControllerClientEntity(
        requestBody: CreateClientDTO,
    ): Promise<ClientEntity> {
        const result = await __request({
            method: 'POST',
            path: `/clients`,
            body: requestBody,
        });
        return result.body;
    }

    /**
     * Create multiple ClientEntities
     * @param requestBody
     * @returns ClientEntity Get create many base response
     * @throws ApiError
     */
    public static async createManyBaseClientControllerClientEntity(
        requestBody: CreateManyClientEntityDto,
    ): Promise<Array<ClientEntity>> {
        const result = await __request({
            method: 'POST',
            path: `/clients/bulk`,
            body: requestBody,
        });
        return result.body;
    }

}