/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { CreateManyPurchaseEntityDto } from '../models/CreateManyPurchaseEntityDto';
import type { GetManyPurchaseEntityResponseDto } from '../models/GetManyPurchaseEntityResponseDto';
import type { PurchaseEntity } from '../models/PurchaseEntity';
import { request as __request } from '../core/request';

export class PurchaseService {

    /**
     * Retrieve a single PurchaseEntity
     * @param id
     * @param fields Selects resource fields. <a href="https://github.com/nestjsx/crud/wiki/Requests#select" target="_blank">Docs</a>
     * @param join Adds relational resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#join" target="_blank">Docs</a>
     * @param cache Reset cache (if was enabled). <a href="https://github.com/nestjsx/crud/wiki/Requests#cache" target="_blank">Docs</a>
     * @returns PurchaseEntity Get one base response
     * @throws ApiError
     */
    public static async getOneBasePurchaseControllerPurchaseEntity(
        id: number,
        fields?: Array<string>,
        join?: Array<string>,
        cache?: number,
    ): Promise<PurchaseEntity> {
        const result = await __request({
            method: 'GET',
            path: `/purchases/${id}`,
            query: {
                'fields': fields,
                'join': join,
                'cache': cache,
            },
        });
        return result.body;
    }

    /**
     * Update a single PurchaseEntity
     * @param id
     * @param requestBody
     * @returns PurchaseEntity Response
     * @throws ApiError
     */
    public static async updateOneBasePurchaseControllerPurchaseEntity(
        id: number,
        requestBody: PurchaseEntity,
    ): Promise<PurchaseEntity> {
        const result = await __request({
            method: 'PATCH',
            path: `/purchases/${id}`,
            body: requestBody,
        });
        return result.body;
    }

    /**
     * Replace a single PurchaseEntity
     * @param id
     * @param requestBody
     * @returns PurchaseEntity Response
     * @throws ApiError
     */
    public static async replaceOneBasePurchaseControllerPurchaseEntity(
        id: number,
        requestBody: PurchaseEntity,
    ): Promise<PurchaseEntity> {
        const result = await __request({
            method: 'PUT',
            path: `/purchases/${id}`,
            body: requestBody,
        });
        return result.body;
    }

    /**
     * Delete a single PurchaseEntity
     * @param id
     * @returns any Delete one base response
     * @throws ApiError
     */
    public static async deleteOneBasePurchaseControllerPurchaseEntity(
        id: number,
    ): Promise<any> {
        const result = await __request({
            method: 'DELETE',
            path: `/purchases/${id}`,
        });
        return result.body;
    }

    /**
     * Retrieve multiple PurchaseEntities
     * @param fields Selects resource fields. <a href="https://github.com/nestjsx/crud/wiki/Requests#select" target="_blank">Docs</a>
     * @param s Adds search condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#search" target="_blank">Docs</a>
     * @param filter Adds filter condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#filter" target="_blank">Docs</a>
     * @param or Adds OR condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#or" target="_blank">Docs</a>
     * @param sort Adds sort by field. <a href="https://github.com/nestjsx/crud/wiki/Requests#sort" target="_blank">Docs</a>
     * @param join Adds relational resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#join" target="_blank">Docs</a>
     * @param limit Limit amount of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#limit" target="_blank">Docs</a>
     * @param offset Offset amount of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#offset" target="_blank">Docs</a>
     * @param page Page portion of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#page" target="_blank">Docs</a>
     * @param cache Reset cache (if was enabled). <a href="https://github.com/nestjsx/crud/wiki/Requests#cache" target="_blank">Docs</a>
     * @returns any Get many base response
     * @throws ApiError
     */
    public static async getManyBasePurchaseControllerPurchaseEntity(
        fields?: Array<string>,
        s?: string,
        filter?: Array<string>,
        or?: Array<string>,
        sort?: Array<string>,
        join?: Array<string>,
        limit?: number,
        offset?: number,
        page?: number,
        cache?: number,
    ): Promise<(GetManyPurchaseEntityResponseDto | Array<PurchaseEntity>)> {
        const result = await __request({
            method: 'GET',
            path: `/purchases`,
            query: {
                'fields': fields,
                's': s,
                'filter': filter,
                'or': or,
                'sort': sort,
                'join': join,
                'limit': limit,
                'offset': offset,
                'page': page,
                'cache': cache,
            },
        });
        return result.body;
    }

    /**
     * Create a single PurchaseEntity
     * @param requestBody
     * @returns PurchaseEntity Get create one base response
     * @throws ApiError
     */
    public static async createOneBasePurchaseControllerPurchaseEntity(
        requestBody: PurchaseEntity,
    ): Promise<PurchaseEntity> {
        const result = await __request({
            method: 'POST',
            path: `/purchases`,
            body: requestBody,
        });
        return result.body;
    }

    /**
     * Create multiple PurchaseEntities
     * @param requestBody
     * @returns PurchaseEntity Get create many base response
     * @throws ApiError
     */
    public static async createManyBasePurchaseControllerPurchaseEntity(
        requestBody: CreateManyPurchaseEntityDto,
    ): Promise<Array<PurchaseEntity>> {
        const result = await __request({
            method: 'POST',
            path: `/purchases/bulk`,
            body: requestBody,
        });
        return result.body;
    }

}