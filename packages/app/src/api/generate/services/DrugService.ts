/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { CreateManyDrugEntityDto } from '../models/CreateManyDrugEntityDto';
import type { DrugEntity } from '../models/DrugEntity';
import type { GetManyDrugEntityResponseDto } from '../models/GetManyDrugEntityResponseDto';
import { request as __request } from '../core/request';

export class DrugService {

    /**
     * Retrieve a single DrugEntity
     * @param id
     * @param fields Selects resource fields. <a href="https://github.com/nestjsx/crud/wiki/Requests#select" target="_blank">Docs</a>
     * @param join Adds relational resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#join" target="_blank">Docs</a>
     * @param cache Reset cache (if was enabled). <a href="https://github.com/nestjsx/crud/wiki/Requests#cache" target="_blank">Docs</a>
     * @returns DrugEntity Get one base response
     * @throws ApiError
     */
    public static async getOneBaseDrugControllerDrugEntity(
        id: number,
        fields?: Array<string>,
        join?: Array<string>,
        cache?: number,
    ): Promise<DrugEntity> {
        const result = await __request({
            method: 'GET',
            path: `/drugs/${id}`,
            query: {
                'fields': fields,
                'join': join,
                'cache': cache,
            },
        });
        return result.body;
    }

    /**
     * Update a single DrugEntity
     * @param id
     * @param requestBody
     * @returns DrugEntity Response
     * @throws ApiError
     */
    public static async updateOneBaseDrugControllerDrugEntity(
        id: number,
        requestBody: DrugEntity,
    ): Promise<DrugEntity> {
        const result = await __request({
            method: 'PATCH',
            path: `/drugs/${id}`,
            body: requestBody,
        });
        return result.body;
    }

    /**
     * Replace a single DrugEntity
     * @param id
     * @param requestBody
     * @returns DrugEntity Response
     * @throws ApiError
     */
    public static async replaceOneBaseDrugControllerDrugEntity(
        id: number,
        requestBody: DrugEntity,
    ): Promise<DrugEntity> {
        const result = await __request({
            method: 'PUT',
            path: `/drugs/${id}`,
            body: requestBody,
        });
        return result.body;
    }

    /**
     * Delete a single DrugEntity
     * @param id
     * @returns any Delete one base response
     * @throws ApiError
     */
    public static async deleteOneBaseDrugControllerDrugEntity(
        id: number,
    ): Promise<any> {
        const result = await __request({
            method: 'DELETE',
            path: `/drugs/${id}`,
        });
        return result.body;
    }

    /**
     * Retrieve multiple DrugEntities
     * @param fields Selects resource fields. <a href="https://github.com/nestjsx/crud/wiki/Requests#select" target="_blank">Docs</a>
     * @param s Adds search condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#search" target="_blank">Docs</a>
     * @param filter Adds filter condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#filter" target="_blank">Docs</a>
     * @param or Adds OR condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#or" target="_blank">Docs</a>
     * @param sort Adds sort by field. <a href="https://github.com/nestjsx/crud/wiki/Requests#sort" target="_blank">Docs</a>
     * @param join Adds relational resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#join" target="_blank">Docs</a>
     * @param limit Limit amount of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#limit" target="_blank">Docs</a>
     * @param offset Offset amount of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#offset" target="_blank">Docs</a>
     * @param page Page portion of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#page" target="_blank">Docs</a>
     * @param cache Reset cache (if was enabled). <a href="https://github.com/nestjsx/crud/wiki/Requests#cache" target="_blank">Docs</a>
     * @returns any Get many base response
     * @throws ApiError
     */
    public static async getManyBaseDrugControllerDrugEntity(
        fields?: Array<string>,
        s?: string,
        filter?: Array<string>,
        or?: Array<string>,
        sort?: Array<string>,
        join?: Array<string>,
        limit?: number,
        offset?: number,
        page?: number,
        cache?: number,
    ): Promise<(GetManyDrugEntityResponseDto | Array<DrugEntity>)> {
        const result = await __request({
            method: 'GET',
            path: `/drugs`,
            query: {
                'fields': fields,
                's': s,
                'filter': filter,
                'or': or,
                'sort': sort,
                'join': join,
                'limit': limit,
                'offset': offset,
                'page': page,
                'cache': cache,
            },
        });
        return result.body;
    }

    /**
     * Create a single DrugEntity
     * @param requestBody
     * @returns DrugEntity Get create one base response
     * @throws ApiError
     */
    public static async createOneBaseDrugControllerDrugEntity(
        requestBody: DrugEntity,
    ): Promise<DrugEntity> {
        const result = await __request({
            method: 'POST',
            path: `/drugs`,
            body: requestBody,
        });
        return result.body;
    }

    /**
     * Create multiple DrugEntities
     * @param requestBody
     * @returns DrugEntity Get create many base response
     * @throws ApiError
     */
    public static async createManyBaseDrugControllerDrugEntity(
        requestBody: CreateManyDrugEntityDto,
    ): Promise<Array<DrugEntity>> {
        const result = await __request({
            method: 'POST',
            path: `/drugs/bulk`,
            body: requestBody,
        });
        return result.body;
    }

}