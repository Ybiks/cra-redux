/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { CreateManyLoyaltySystemEntityDto } from '../models/CreateManyLoyaltySystemEntityDto';
import type { GetManyLoyaltySystemEntityResponseDto } from '../models/GetManyLoyaltySystemEntityResponseDto';
import type { LoyaltySystemEntity } from '../models/LoyaltySystemEntity';
import { request as __request } from '../core/request';

export class LoyaltySystemService {

    /**
     * Retrieve a single LoyaltySystemEntity
     * @param id
     * @param fields Selects resource fields. <a href="https://github.com/nestjsx/crud/wiki/Requests#select" target="_blank">Docs</a>
     * @param join Adds relational resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#join" target="_blank">Docs</a>
     * @param cache Reset cache (if was enabled). <a href="https://github.com/nestjsx/crud/wiki/Requests#cache" target="_blank">Docs</a>
     * @returns LoyaltySystemEntity Get one base response
     * @throws ApiError
     */
    public static async getOneBaseLoyaltySystemControllerLoyaltySystemEntity(
        id: number,
        fields?: Array<string>,
        join?: Array<string>,
        cache?: number,
    ): Promise<LoyaltySystemEntity> {
        const result = await __request({
            method: 'GET',
            path: `/loyalty-system/${id}`,
            query: {
                'fields': fields,
                'join': join,
                'cache': cache,
            },
        });
        return result.body;
    }

    /**
     * Update a single LoyaltySystemEntity
     * @param id
     * @param requestBody
     * @returns LoyaltySystemEntity Response
     * @throws ApiError
     */
    public static async updateOneBaseLoyaltySystemControllerLoyaltySystemEntity(
        id: number,
        requestBody: LoyaltySystemEntity,
    ): Promise<LoyaltySystemEntity> {
        const result = await __request({
            method: 'PATCH',
            path: `/loyalty-system/${id}`,
            body: requestBody,
        });
        return result.body;
    }

    /**
     * Replace a single LoyaltySystemEntity
     * @param id
     * @param requestBody
     * @returns LoyaltySystemEntity Response
     * @throws ApiError
     */
    public static async replaceOneBaseLoyaltySystemControllerLoyaltySystemEntity(
        id: number,
        requestBody: LoyaltySystemEntity,
    ): Promise<LoyaltySystemEntity> {
        const result = await __request({
            method: 'PUT',
            path: `/loyalty-system/${id}`,
            body: requestBody,
        });
        return result.body;
    }

    /**
     * Delete a single LoyaltySystemEntity
     * @param id
     * @returns any Delete one base response
     * @throws ApiError
     */
    public static async deleteOneBaseLoyaltySystemControllerLoyaltySystemEntity(
        id: number,
    ): Promise<any> {
        const result = await __request({
            method: 'DELETE',
            path: `/loyalty-system/${id}`,
        });
        return result.body;
    }

    /**
     * Retrieve multiple LoyaltySystemEntities
     * @param fields Selects resource fields. <a href="https://github.com/nestjsx/crud/wiki/Requests#select" target="_blank">Docs</a>
     * @param s Adds search condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#search" target="_blank">Docs</a>
     * @param filter Adds filter condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#filter" target="_blank">Docs</a>
     * @param or Adds OR condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#or" target="_blank">Docs</a>
     * @param sort Adds sort by field. <a href="https://github.com/nestjsx/crud/wiki/Requests#sort" target="_blank">Docs</a>
     * @param join Adds relational resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#join" target="_blank">Docs</a>
     * @param limit Limit amount of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#limit" target="_blank">Docs</a>
     * @param offset Offset amount of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#offset" target="_blank">Docs</a>
     * @param page Page portion of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#page" target="_blank">Docs</a>
     * @param cache Reset cache (if was enabled). <a href="https://github.com/nestjsx/crud/wiki/Requests#cache" target="_blank">Docs</a>
     * @returns any Get many base response
     * @throws ApiError
     */
    public static async getManyBaseLoyaltySystemControllerLoyaltySystemEntity(
        fields?: Array<string>,
        s?: string,
        filter?: Array<string>,
        or?: Array<string>,
        sort?: Array<string>,
        join?: Array<string>,
        limit?: number,
        offset?: number,
        page?: number,
        cache?: number,
    ): Promise<(GetManyLoyaltySystemEntityResponseDto | Array<LoyaltySystemEntity>)> {
        const result = await __request({
            method: 'GET',
            path: `/loyalty-system`,
            query: {
                'fields': fields,
                's': s,
                'filter': filter,
                'or': or,
                'sort': sort,
                'join': join,
                'limit': limit,
                'offset': offset,
                'page': page,
                'cache': cache,
            },
        });
        return result.body;
    }

    /**
     * Create a single LoyaltySystemEntity
     * @param requestBody
     * @returns LoyaltySystemEntity Get create one base response
     * @throws ApiError
     */
    public static async createOneBaseLoyaltySystemControllerLoyaltySystemEntity(
        requestBody: LoyaltySystemEntity,
    ): Promise<LoyaltySystemEntity> {
        const result = await __request({
            method: 'POST',
            path: `/loyalty-system`,
            body: requestBody,
        });
        return result.body;
    }

    /**
     * Create multiple LoyaltySystemEntities
     * @param requestBody
     * @returns LoyaltySystemEntity Get create many base response
     * @throws ApiError
     */
    public static async createManyBaseLoyaltySystemControllerLoyaltySystemEntity(
        requestBody: CreateManyLoyaltySystemEntityDto,
    ): Promise<Array<LoyaltySystemEntity>> {
        const result = await __request({
            method: 'POST',
            path: `/loyalty-system/bulk`,
            body: requestBody,
        });
        return result.body;
    }

}