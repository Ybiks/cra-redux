/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ClientEntity } from './ClientEntity';

export type GetManyClientEntityResponseDto = {
    data: Array<ClientEntity>;
    count: number;
    total: number;
    page: number;
    pageCount: number;
}
