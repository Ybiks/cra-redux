/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { LoyaltySystemEntity } from './LoyaltySystemEntity';
import type { PurchaseEntity } from './PurchaseEntity';

export type ClientEntity = {
    readonly id: number;
    name: string;
    address: string;
    readonly purchases: Array<PurchaseEntity>;
    readonly loyaltySystem?: LoyaltySystemEntity;
    readonly createdAt: number;
}
