/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { PurchaseEntity } from './PurchaseEntity';

export type CreateManyPurchaseEntityDto = {
    bulk: Array<PurchaseEntity>;
}
