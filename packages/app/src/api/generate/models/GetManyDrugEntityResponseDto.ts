/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { DrugEntity } from './DrugEntity';

export type GetManyDrugEntityResponseDto = {
    data: Array<DrugEntity>;
    count: number;
    total: number;
    page: number;
    pageCount: number;
}
