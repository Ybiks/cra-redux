/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type DrugEntity = {
    readonly id: number;
    type: string;
    purchases: Array<string>;
}
