/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { CreateClientDTO } from './CreateClientDTO';

export type CreateManyClientEntityDto = {
    bulk: Array<CreateClientDTO>;
}
