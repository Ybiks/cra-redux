/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { DrugEntity } from './DrugEntity';

export type CreateManyDrugEntityDto = {
    bulk: Array<DrugEntity>;
}
