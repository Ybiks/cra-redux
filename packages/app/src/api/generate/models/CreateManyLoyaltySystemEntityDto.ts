/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { LoyaltySystemEntity } from './LoyaltySystemEntity';

export type CreateManyLoyaltySystemEntityDto = {
    bulk: Array<LoyaltySystemEntity>;
}
