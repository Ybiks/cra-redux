/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { LoyaltySystemEntity } from './LoyaltySystemEntity';

export type GetManyLoyaltySystemEntityResponseDto = {
    data: Array<LoyaltySystemEntity>;
    count: number;
    total: number;
    page: number;
    pageCount: number;
}
