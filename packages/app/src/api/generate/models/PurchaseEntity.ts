/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type PurchaseEntity = {
    readonly id: number;
    client: any;
    clientId: number;
    drug: any;
    drugId: number;
    weight: number;
    createdAt: number;
}
