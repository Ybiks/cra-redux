/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { PurchaseEntity } from './PurchaseEntity';

export type GetManyPurchaseEntityResponseDto = {
    data: Array<PurchaseEntity>;
    count: number;
    total: number;
    page: number;
    pageCount: number;
}
