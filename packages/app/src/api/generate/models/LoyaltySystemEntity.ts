/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type LoyaltySystemEntity = {
    readonly id: number;
    clientId: number;
    usedCoupons: Array<string>;
    bonuses: number;
}
