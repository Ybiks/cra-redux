import {
  ClientService,
  PurchaseService,
  DrugService,
  OpenAPI,
  ClientEntity,
  PurchaseEntity,
  DrugEntity,
} from "./generate";

OpenAPI.BASE = "http://localhost:8080"; // TODO: .env

export const matchingMethods = {
  client: {
    getAll: ClientService.getManyBaseClientControllerClientEntity,
  },
  purchase: {
    getAll: PurchaseService.getManyBasePurchaseControllerPurchaseEntity,
  },
  drug: {
    getAll: DrugService.getManyBaseDrugControllerDrugEntity,
  },
};

type TableEntities =
  | (ClientEntity & { type: "client" })
  | (PurchaseEntity & { type: "purchase" })
  | (DrugEntity & { type: "drug" });

export type TableTypes = TableEntities["type"];

export type TableFields<
  T extends TableTypes,
  P extends TableEntities = TableEntities
> = {
  type: P;
} extends { type: infer A }
  ? A extends { type: T }
    ? Omit<A, "type">
    : never
  : never;
