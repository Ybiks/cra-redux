import styled from 'styled-components';

import { Box } from '../atoms';

export const Control = styled(Box)`
  background-color: rgba(0, 0, 0, 0);
  transition: background 0.3s;
  outline: none;
  border: none;
  font-size: 0;
`;

Control.defaultProps = {
  backgroundColor: 'background100',
};
