import styled from 'styled-components';

import { Box, BoxProps } from '../atoms';

const FilterInput = styled(Box)`
  border: none;
  width: 100%;
  border-bottom: 1px solid ${({ theme }) => theme.colors.background200};
  background-color: rgba(0, 0, 0, 0);
  box-shadow: none;
  border-radius: 0;
  transition: 0.3s;
  color: ${({ theme }) => theme.colors.secondary100};

  &:hover:not(:disabled) {
    border-color: ${({ theme }) => theme.colors.primary100};
  }

  &:focus {
    outline: none;
    background: transparent;
    border-color: ${({ theme }) => theme.colors.primary100};
  }
`;

export type FilterFieldProps = { wrapperProps?: BoxProps; placeholder: string } & BoxProps &
  React.HTMLProps<HTMLInputElement>;

export const FilterField = ({ wrapperProps, placeholder, ...filterProps }: FilterFieldProps) => (
  <Box as="label" fontSize={0} width="100%" {...wrapperProps}>
    {placeholder}
    <FilterInput as="input" placeholder={placeholder} {...filterProps} />
  </Box>
);

FilterField.defaultProps = {
  minHeight: [48, 37],
  textStyle: 'bs.label350',
  fontSize: [16, 14],
  width: '100%',
  pl: 2,
};
