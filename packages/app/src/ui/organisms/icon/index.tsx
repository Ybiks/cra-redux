import styled from 'styled-components';
import { compose, size, SizeProps, space, SpaceProps } from 'styled-system';

import { DefaultSprite } from './sprite';

import iconsStripe from './sprite.svg';

const composedHelpers = compose(size, space);

const StyledSvg = styled.svg`
  display: inline-flex;
  ${composedHelpers}
`;

type IconProps<T extends string | undefined> = (T extends string
  ? { src: string; name?: string }
  : { src?: undefined; name: DefaultSprite }) & { className?: string } & SizeProps &
  SpaceProps;

export function Icon<T extends string | undefined>({
  src,
  name,
  className = 'secondary200-fill',
  ...rest
}: IconProps<T>) {
  return (
    <StyledSvg focusable="false" aria-hidden="true" role="presentation" {...rest}>
      <use className={className} xlinkHref={`${src || iconsStripe}${name ? `#${name}` : ''}`} />
    </StyledSvg>
  );
}

Icon.defaultProps = {
  size: 24,
};
