export type DefaultSprite =
  | 'alphabet'
  | 'arrow'
  | 'exit'
  | 'moon'
  | 'search'
  | 'answer-sidebar'
  | 'sun';
