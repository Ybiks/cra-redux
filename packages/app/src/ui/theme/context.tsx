import { FC, useCallback, useEffect, useState } from "react";
import constate from "constate";
import { ThemeProvider } from "styled-components";

import { darkTheme, lightTheme } from "./variants";

type Themes = "light" | "dark";

const storeKey = "_theme";

const ThemeMode = () => {
  const [themeMode, setThemeMode] = useState<Themes>("light");

  const handleToggleThemeMode = useCallback(() => {
    setThemeMode((value) => {
      const nextValue: Themes = value === "light" ? "dark" : "light";
      window.localStorage.setItem(storeKey, nextValue);
      return nextValue;
    });
  }, []);

  useEffect(() => {
    const value = window.localStorage.getItem(storeKey) as Themes | null;
    if (value && ["light", "dark"].includes(value)) {
      setThemeMode(value);
      return;
    }

    if (window.matchMedia("(prefers-color-scheme: dark)").matches) {
      setThemeMode("dark");
      return;
    }

    setThemeMode("light");
  }, []);

  return { themeMode, onToggleThemeMode: handleToggleThemeMode };
};

export const [ThemeModeProvider, useThemeModeValue, useThemeMode] = constate(
  ThemeMode,
  (value) => value.themeMode,
  (value) => value
);

export const CustomizedThemeProvider: FC = ({ children }) => {
  const themeMode = useThemeModeValue();

  return (
    <ThemeProvider theme={themeMode === "light" ? lightTheme : darkTheme}>
      {children}
    </ThemeProvider>
  );
};
