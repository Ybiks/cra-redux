import { darkColors, lightColors } from './theme-colors';
import { textStyles } from './theme-fonts';

const fonts = {
  primary: "'Open Sans', sans-serif",
};

const fontSizes = {
  subheadline: '24px',
  body: '20px',
  small: '12px',
};

const lineHeights = { default: 1.5 };

const fontWeights = {
  b: 700,
  sb: 600,
  m: 500,
  r: 400,
};

export const breakpoints = ['768px', '1024px', '1440px', '1920px'];
export const breakpointsReverseCheckOrder = ['767px', '1023px', '1439px'];

export const themeSpace = [0, 8, 16, 24, 32, 40, 48, 56, 64, 72, 80, 88, 96];

export const lightTheme = {
  breakpoints,
  colors: lightColors,
  fonts,
  fontSizes,
  lineHeights,
  fontWeights,
  radii: {
    default: '4px',
  },
  space: themeSpace,
  textStyles,
} as const;

export const darkTheme = {
  breakpoints,
  colors: darkColors,
  fonts,
  fontSizes,
  lineHeights,
  fontWeights,
  radii: {
    default: '4px',
  },
  space: themeSpace,
  textStyles,
} as const;

export type Theme = typeof lightTheme | typeof darkTheme;
