export const lightColors = {
  primary100: "#25e65f",
  primary200: "#099634",

  secondary100: "#1f1333",
  secondary200: "#555",
  // secondary100: '#6A6B8A',

  background100: "#ffffff",
  background200: "#d2d0d6",
  // background200: '#f4f4f4',
  background300: "#e8ebfc",
};

export const darkColors = {
  primary100: "#25e65f",
  primary200: "#099634",

  secondary100: "#bfbfbf",
  secondary200: "#999",

  background100: "#222",
  background200: "#444",
  background300: "#666",
};

export type ThemeColors = keyof typeof darkColors;

export type colorsType =
  | "colors.primary100"
  | "colors.primary120"
  | "colors.primary200"
  | "colors.secondary100"
  | "colors.secondary200"
  | "colors.background100"
  | "colors.background200"
  | "colors.background300";
