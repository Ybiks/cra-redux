import { createGlobalStyle, css } from 'styled-components';

// import './global.css';

const styles = css`
  html,
  body {
    display: flex;
    margin: 0;
    padding: 0;
    width: 100%;
    height: 100%;
    font-weight: 400;
    background-color: ${({ theme }) => theme.colors.background100};
    color: ${({ theme }) => theme.colors.secondary100};
    transition: background 0.3s;
  }

  .react-tooltip-lite {
    color: ${({ theme }) => theme.colors.secondary100};
    font-family: Open Sans, OpenSans-Bold, sans-serif;
  }

  .react-tooltip-lite-arrow {
    display: none;
  }

  #__next {
    display: flex;
    flex: 1 auto;
  }

  /* width */
  .custom-scroll::-webkit-scrollbar {
    width: 2px;
  }

  /* Track */
  .custom-scroll::-webkit-scrollbar-track {
    background: ${({ theme }) => theme.colors.background200};
  }

  /* Handle */
  .custom-scroll::-webkit-scrollbar-thumb,
  .custom-scroll::-webkit-scrollbar-thumb:hover {
    background: ${({ theme }) => theme.colors.background300};
  }

  .primary100-fill {
    fill: ${({ theme }) => theme.colors.primary100};
  }

  .secondary100-fill {
    fill: ${({ theme }) => theme.colors.secondary100};
  }

  .secondary200-fill {
    fill: ${({ theme }) => theme.colors.secondary200};
  }
`;

export const GlobalStyles = createGlobalStyle`${styles}`;
