export const textStyles = {
  md: {
    label500: { font: '700 16px / 1.50  Open Sans, OpenSans-Bold, sans-serif' },
    label300: { font: '400 16px / 1.50  Open Sans, OpenSans-Regular, sans-serif' },
    h700: { font: '700 20px / 1.50  OpenSans-Bold, sans-serif' },
    input700: { font: '400 24px / 1.40  Open Sans, OpenSans-Regular, sans-serif' },
    h900: { font: '700 32px / 1.40  Open Sans, OpenSans-Bold, sans-serif' },
    label800: { font: '700 20px / 1.50  Open Sans, OpenSans-Bold, sans-serif' },
    label400: { font: '400 18px / 1.50  Open Sans, OpenSans-Regular, sans-serif' },
    label600: { font: '700 18px / 1.50  Open Sans, OpenSans-Bold, sans-serif' },
    input900: { font: '400 56px / 1.40  Open Sans, OpenSans-Regular, sans-serif' },
    label650: { font: '400 20px / 1.50  Open Sans, OpenSans-Regular, sans-serif' },
    h990: { font: '700 56px / 1.20  Open Sans, OpenSans-Bold, sans-serif' },
  },
  bs: {
    label150: { font: '700 10px / 1.50  Open Sans, OpenSans-Bold, sans-serif' },
    label350: { font: '400 14px / 1.50  Open Sans, OpenSans-Regular, sans-serif' },
    label600: { font: '700 12px / 1.50  Open Sans, OpenSans-Bold, sans-serif' },
    h900: { font: '700 24px / 1.40  OpenSans-Bold, sans-serif' },
    label300: { font: '400 12px / 1.50  Open Sans, OpenSans-Regular, sans-serif' },
    h800: { font: '700 16px / 1.40  OpenSans-Bold, sans-serif' },
    input700: { font: '400 12px / 1.50  Open Sans, OpenSans-Regular, sans-serif' },
    label100: { font: '400 10px / 1.50  Open Sans, OpenSans-Regular, sans-serif' },
    input900: { font: '400 32px / 1.40  Open Sans, OpenSans-Regular, sans-serif' },
  },
};
export type textStylesType =
  | 'md.label500'
  | 'md.label300'
  | 'bs.label150'
  | 'md.h700'
  | 'bs.label350'
  | 'md.input700'
  | 'md.h900'
  | 'md.label800'
  | 'bs.label600'
  | 'md.label400'
  | 'md.label600'
  | 'bs.h900'
  | 'bs.label300'
  | 'bs.h800'
  | 'bs.input700'
  | 'md.input900'
  | 'md.input700'
  | 'md.label650'
  | 'bs.label100'
  | 'bs.input900'
  | 'md.h990';
