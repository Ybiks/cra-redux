import { useEffect, useRef, useState } from "react";

import { breakpointsReverseCheckOrder } from "./variants";

export type UseMediaQueryResult = {
  phone?: boolean;
  tablet?: boolean;
  desktop?: boolean;
  desktopHD?: boolean;
};

const getDevice = (): UseMediaQueryResult => {
  const phone = window.matchMedia(
    `(max-width: ${breakpointsReverseCheckOrder[0]})`
  ).matches;
  if (phone) return { phone };

  const tablet = window.matchMedia(
    `(max-width: ${breakpointsReverseCheckOrder[1]})`
  ).matches;
  if (tablet) return { tablet };

  const desktop = window.matchMedia(
    `(max-width: ${breakpointsReverseCheckOrder[2]})`
  ).matches;
  if (desktop) return { desktop };

  return { desktopHD: true };
};

export const useMediaQuery = (): UseMediaQueryResult => {
  const [result, setResult] = useState(getDevice);
  const resultRef = useRef(result);

  useEffect(() => {
    const handler = () => {
      const newResult = getDevice();
      if (
        Object.entries(newResult).some(
          ([key, value]) =>
            resultRef.current[key as keyof UseMediaQueryResult] !== value
        )
      ) {
        setResult(newResult);
      }
    };

    window.addEventListener("resize", handler);

    return () => window.removeEventListener("resize", handler);
  }, []);

  return result;
};
