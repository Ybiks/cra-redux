import styled from 'styled-components';
import {
  alignItems,
  AlignItemsProps,
  alignSelf,
  AlignSelfProps,
  compose,
  flex,
  flexDirection,
  FlexDirectionProps,
  flexGrow,
  FlexGrowProps,
  FlexProps as StyledFlexProps,
  flexShrink,
  FlexShrinkProps,
  flexWrap,
  FlexWrapProps,
  justifyContent,
  JustifyContentProps,
  justifySelf,
  JustifySelfProps,
} from 'styled-system';

import { Box, BoxProps } from '../box';

export type FlexProps = AlignItemsProps &
  AlignSelfProps &
  FlexDirectionProps &
  FlexGrowProps &
  StyledFlexProps &
  FlexShrinkProps &
  FlexWrapProps &
  JustifyContentProps &
  JustifySelfProps &
  BoxProps;

export const Flex = styled(Box)<FlexProps>(
  compose(
    alignItems,
    alignSelf,
    flexDirection,
    flexGrow,
    flex,
    flexShrink,
    flexWrap,
    justifyContent,
    justifySelf,
  ),
);

Flex.defaultProps = { display: 'flex' };
