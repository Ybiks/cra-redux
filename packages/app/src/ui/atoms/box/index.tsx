import styled, { CSSProperties } from "styled-components";
import {
  BackgroundColorProps,
  border,
  BorderProps,
  color,
  ColorProps,
  compose,
  display,
  DisplayProps,
  flex,
  FlexProps,
  height,
  HeightProps,
  maxHeight,
  MaxHeightProps,
  maxWidth,
  MaxWidthProps,
  minHeight,
  MinHeightProps,
  minWidth,
  MinWidthProps,
  overflow,
  OverflowProps,
  position,
  PositionProps,
  space,
  SpaceProps,
  system,
  textStyle,
  TextStyleProps,
  typography,
  TypographyProps,
  width,
  WidthProps,
} from "styled-system";

import { textStylesType, ThemeColors } from "../../theme";

export type BoxColors = ThemeColors | "transparent" | "none";

export type BoxProps = HeightProps &
  MaxHeightProps &
  MaxWidthProps &
  WidthProps &
  PositionProps &
  BackgroundColorProps &
  DisplayProps &
  FlexProps &
  MinHeightProps &
  MinWidthProps &
  OverflowProps &
  TypographyProps &
  Omit<BorderProps, "borderColor"> &
  Omit<ColorProps, "backgroundColor" | "color" | "bg"> &
  Omit<TextStyleProps, "textStyle"> &
  SpaceProps & {
    bg?: (ColorProps["bg"] & BoxColors) | BoxColors[];
    backgroundColor?: BoxColors | BoxColors[];
    color?: (ColorProps["color"] & BoxColors) | BoxColors[];
    borderColor?: BoxColors | BoxColors[];
    as?: keyof JSX.IntrinsicElements | React.ComponentType<any>;
    cursor?: CSSProperties["cursor"];
    textStyle?: textStylesType | textStylesType[];
    upFirstLetter?: boolean;
  };

const custom = system({
  cursor: {
    property: "cursor",
  },
});

export const Box = styled("div")<BoxProps>(
  compose(
    typography,
    textStyle,
    space,
    color,
    width,
    display,
    flex,
    position,
    minHeight,
    minWidth,
    overflow,
    height,
    maxWidth,
    border,
    maxHeight,
    custom
  )
);
