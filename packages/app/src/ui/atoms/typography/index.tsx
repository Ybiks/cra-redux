import styled from "styled-components";
import { compose, textAlign, TextAlignProps } from "styled-system";

import { Box, BoxProps } from "../box";

export type TypographyProps = BoxProps &
  TextAlignProps & { upFirstLetter?: boolean };

export const Typography = styled(Box)<TypographyProps>`
  ${compose(textAlign)}
  ${({ upFirstLetter }) =>
    upFirstLetter ? `&::first-letter { text-transform: uppercase; }` : ""}
`;

Typography.defaultProps = { as: "p" };
