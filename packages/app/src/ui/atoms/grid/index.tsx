import styled from "styled-components";
import { compose, GridProps as StyledGridProps, grid } from "styled-system";

import { Box, BoxProps } from "../box";

export type GridProps = StyledGridProps & BoxProps;

export const Grid = styled(Box)<GridProps>(compose(grid));

Grid.defaultProps = { display: "grid" };
