import { DataGrid } from "../data-grid";

export const ClientPage = () => {
  return (
    <DataGrid
      table="client"
      columns={[
        { key: "id" },
        { key: "name", title: "Client name", isSortable: true },
        { key: "address" },
        { key: "createdAt", isSortable: true },
      ]}
    />
  );
};
