import React, { useEffect, useMemo } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Dispatch } from "redux";
import { TableFields, TableTypes } from "../../api";
import {
  FetchTableDataAction,
  tableActions,
} from "../../store/table/saga/actions";
import { Grid, Box, Typography } from "../../ui";

type TableColumnSchema<T extends TableTypes> = {
  key: keyof TableFields<T>;
  title?: string;
  isSortable?: boolean;
};

type DataGridProps<T extends TableTypes> = {
  table: T;
  columns: TableColumnSchema<T>[];
};

export const DataGrid = <T extends TableTypes>({
  table,
  columns,
}: DataGridProps<T>) => {
  const dispatch = useDispatch<Dispatch<FetchTableDataAction>>();
  const tableState = useSelector((state) => state.table);

  const data = useMemo(() => {
    if (table !== tableState.key) return null;

    return tableState.data;
  }, [table, tableState]);

  useEffect(() => {
    if (!data)
      dispatch({
        type: tableActions.FETCH_TABLE_DATA,
        payload: { table },
      });
  }, [data, dispatch, table]);

  return (
    <Grid gridTemplateColumns={`repeat(${columns.length}, 1fr)`} p={2}>
      {columns.map(({ title, key }) => (
        <Typography
          key={String(key)}
          bg="primary100"
          px={2}
          py={1}
          upFirstLetter
        >
          <strong>{title || key}</strong>
        </Typography>
      ))}
      {(data || []).map((item) => (
        <React.Fragment key={(item as any).id || JSON.stringify(item)}>
          {columns.map(({ key }) => (
            <Box key={(key as any) || JSON.stringify(key)} px={2} py={1}>
              {(item as any)[key]}
            </Box>
          ))}
        </React.Fragment>
      ))}
    </Grid>
  );
};
