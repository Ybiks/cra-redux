import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import { ClientPage } from "../client";
import { DrugPage } from "../drug";
import { PurchasePage } from "../purchase";

export const Routing = () => {
  return (
    <Router>
      <Switch>
        <Route path="/clients">
          <ClientPage />
        </Route>
        <Route path="/purchases">
          <PurchasePage />
        </Route>
        <Route path="/drug">
          <DrugPage />
        </Route>
        <Redirect to="/clients" />
      </Switch>
    </Router>
  );
};
