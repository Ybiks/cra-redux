import { LoyaltySystem } from './loyalty-system';
import { Purchase } from './purchase';

export type Client = {
  name: string;
  address: string;
  purchases: Purchase[];
  loyaltySystem: LoyaltySystem;
  createdAt: number;
};
