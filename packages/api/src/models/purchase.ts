export type Purchase = {
  clientId: number;
  drugId: number;
  weight: number;
  createdAt: number;
};
