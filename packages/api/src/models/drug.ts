export type DrugType = 'Cocaine' | 'DMT' | 'GHB' | 'Heroin';

export type Drug = {
  type: DrugType;
};
