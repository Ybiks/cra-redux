export type LoyaltySystem = {
  id: number;
  clientId: number;
  usedCoupons: string[];
  bonuses: number;
};
