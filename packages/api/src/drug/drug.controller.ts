import { Controller } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Crud, CrudController } from '@nestjsx/crud';
import { DrugEntity } from 'src/drug/drug.entity';
import { DrugService } from './drug.service';

@Crud({
  model: {
    type: DrugEntity,
  },
})
@ApiTags('Drug')
@Controller('drugs')
export class DrugController implements CrudController<DrugEntity> {
  constructor(public service: DrugService) {}
}
