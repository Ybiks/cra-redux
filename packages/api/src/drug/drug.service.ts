import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { DrugEntity } from 'src/drug/drug.entity';

@Injectable()
export class DrugService extends TypeOrmCrudService<DrugEntity> {
  constructor(@InjectRepository(DrugEntity) repo) {
    super(repo);
  }
}
