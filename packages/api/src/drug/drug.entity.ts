import { ApiProperty } from '@nestjs/swagger';
import {
  BaseEntity,
  Column,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

import { Drug, DrugType, Purchase } from '../models';
import { PurchaseEntity } from '../purchase/purchase.entity';

@Entity('drug')
export class DrugEntity extends BaseEntity implements Drug {
  @ApiProperty({ readOnly: true })
  @PrimaryGeneratedColumn({ type: 'int' })
  id: number;

  @ApiProperty()
  @Column()
  type: DrugType;

  @ApiProperty({ type: PurchaseEntity })
  @OneToMany(() => PurchaseEntity, (purchase) => purchase.drug)
  purchases: Purchase[];
}
