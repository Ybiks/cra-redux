import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { configService } from './config/config.service';
import { ConfigModuleInstance } from './config/config.module';
import { ClientModule } from './client/client.module';
import { PurchaseModule } from './purchase/purchase.module';
import { DrugModule } from './drug/drug.module';
import { LoyaltySystemModule } from './loyalty-system/loyalty-system.module';

@Module({
  imports: [
    ConfigModuleInstance,

    TypeOrmModule.forRoot(configService.getTypeOrmConfig()),

    ClientModule,

    PurchaseModule,

    DrugModule,

    LoyaltySystemModule,
  ],
})
export class AppModule {}
