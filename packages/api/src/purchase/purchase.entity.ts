import { ApiProperty } from '@nestjs/swagger';
import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

import { Purchase, Drug, Client } from '../models';
import { ClientEntity } from '../client/client.entity';
import { DrugEntity } from '../drug/drug.entity';

@Entity('subject')
export class PurchaseEntity extends BaseEntity implements Purchase {
  @ApiProperty({ readOnly: true })
  @PrimaryGeneratedColumn({ type: 'int8' })
  id: number;

  @ApiProperty()
  @ManyToOne(() => ClientEntity, (client) => client.purchases)
  @JoinColumn({ name: 'clientId' })
  client: Client;

  @ApiProperty()
  @Column()
  clientId: number;

  @ApiProperty()
  @ManyToOne(() => DrugEntity, (drug) => drug.purchases)
  @JoinColumn({ name: 'drugId' })
  drug: Drug;

  @ApiProperty()
  @Column()
  drugId: number;

  @ApiProperty()
  @Column()
  weight: number;

  @ApiProperty()
  @CreateDateColumn()
  createdAt: number;
}
