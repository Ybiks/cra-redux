import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { PurchaseEntity } from 'src/purchase/purchase.entity';

@Injectable()
export class PurchaseService extends TypeOrmCrudService<PurchaseEntity> {
  constructor(@InjectRepository(PurchaseEntity) repo) {
    super(repo);
  }
}
