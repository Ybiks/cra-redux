import { Controller } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Crud, CrudController } from '@nestjsx/crud';
import { PurchaseEntity } from 'src/purchase/purchase.entity';
import { PurchaseService } from './purchase.service';

@Crud({
  model: {
    type: PurchaseEntity,
  },
})
@ApiTags('Purchase')
@Controller('purchases')
export class PurchaseController implements CrudController<PurchaseEntity> {
  constructor(public service: PurchaseService) {}
}
