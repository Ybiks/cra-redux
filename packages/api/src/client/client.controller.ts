import { Controller } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Crud, CrudController } from '@nestjsx/crud';
import { ClientEntity } from 'src/client/client.entity';
import {
  CreateClientDTO,
  GetClientDTO,
  GetManyClientDTO,
  UpdateClientDTO,
} from './client.dto';
import { ClientService } from './client.service';

@Crud({
  model: {
    type: ClientEntity,
  },
  dto: {
    create: CreateClientDTO,
    update: UpdateClientDTO,
  },
  serialize: {
    get: GetClientDTO,
    getMany: GetManyClientDTO,
  },
})
@ApiTags('Client')
@Controller('clients')
export class ClientController implements CrudController<ClientEntity> {
  constructor(public service: ClientService) {}
}
