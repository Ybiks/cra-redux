import { Client, LoyaltySystem, Purchase } from 'src/models';

export class SharedClientDTO implements Pick<Client, 'name' | 'address'> {
  name: string;
  address: string;
}

export class CreateClientDTO extends SharedClientDTO {}
export class UpdateClientDTO extends SharedClientDTO {}

export class GetManyClientDTO
  extends SharedClientDTO
  implements Omit<Client, 'purchases'>
{
  loyaltySystem: LoyaltySystem;
  createdAt: number;
}

export class GetClientDTO extends SharedClientDTO implements Client {
  loyaltySystem: LoyaltySystem;
  purchases: Purchase[];
  createdAt: number;
}
