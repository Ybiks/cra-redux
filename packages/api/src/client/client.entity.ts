import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { IsString, MaxLength, IsNotEmpty } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

import { Client, Purchase } from '../models';
import { PurchaseEntity } from '../purchase/purchase.entity';
import { LoyaltySystemEntity } from '../loyalty-system/loyalty-system.entity';

@Entity('client')
export class ClientEntity extends BaseEntity implements Client {
  @ApiProperty({ readOnly: true })
  @PrimaryGeneratedColumn({ type: 'int4' })
  id: number;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  @MaxLength(50, { always: true })
  @Column()
  name: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  @MaxLength(100, { always: true })
  @Column()
  address: string;

  @ApiProperty({ isArray: true, type: PurchaseEntity, readOnly: true })
  @OneToMany(() => PurchaseEntity, (purchase) => purchase.client)
  purchases: Purchase[];

  @ApiProperty({ type: LoyaltySystemEntity, readOnly: true, required: false })
  @OneToOne(() => LoyaltySystemEntity, (loyaltySystem) => loyaltySystem.client)
  loyaltySystem: LoyaltySystemEntity;

  @ApiProperty({ readOnly: true })
  @CreateDateColumn()
  createdAt: number;
}
