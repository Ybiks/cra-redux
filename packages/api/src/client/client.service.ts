import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { ClientEntity } from 'src/client/client.entity';

@Injectable()
export class ClientService extends TypeOrmCrudService<ClientEntity> {
  constructor(@InjectRepository(ClientEntity) repo) {
    super(repo);
  }
}
