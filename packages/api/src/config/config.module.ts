import { ConfigModule } from '@nestjs/config';
import * as path from 'path';

const { PWD } = process.env;

export const ConfigModuleInstance = ConfigModule.forRoot({
  envFilePath: path.join(PWD, '../../.env'),
  isGlobal: true,
});
