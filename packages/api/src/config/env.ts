import * as dotenv from 'dotenv';
import * as path from 'path';

const { PWD } = process.env;

dotenv.config({ path: path.join(PWD, '../../.env') });
