import { IsArray, IsNotEmpty } from 'class-validator';
import { LoyaltySystem } from 'src/models';

export class SharedLoyaltySystemDTO
  implements Pick<LoyaltySystem, 'usedCoupons' | 'bonuses'>
{
  @IsArray()
  @IsNotEmpty()
  usedCoupons: string[];

  @IsNotEmpty()
  bonuses: number;
}

export class CreateLoyaltySystemDTO extends SharedLoyaltySystemDTO {}
export class UpdateLoyaltySystemDTO extends SharedLoyaltySystemDTO {}
