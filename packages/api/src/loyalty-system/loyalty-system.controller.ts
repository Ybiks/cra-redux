import { Controller } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Crud, CrudController } from '@nestjsx/crud';
import { LoyaltySystemEntity } from 'src/loyalty-system/loyalty-system.entity';
import { CreateLoyaltySystemDTO } from './loyalty-system.dto';
import { LoyaltySystemService } from './loyalty-system.service';

@Crud({
  model: {
    type: LoyaltySystemEntity,
  },
  dto: {
    create: CreateLoyaltySystemDTO,
  },
})
@ApiTags('Loyalty system')
@Controller('loyalty-system')
export class LoyaltySystemController
  implements CrudController<LoyaltySystemEntity>
{
  constructor(public service: LoyaltySystemService) {}
}
