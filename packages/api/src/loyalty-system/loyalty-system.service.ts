import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { LoyaltySystemEntity } from 'src/loyalty-system/loyalty-system.entity';

@Injectable()
export class LoyaltySystemService extends TypeOrmCrudService<LoyaltySystemEntity> {
  constructor(@InjectRepository(LoyaltySystemEntity) repo) {
    super(repo);
  }
}
