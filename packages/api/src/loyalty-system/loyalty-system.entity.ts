import {
  BaseEntity,
  Column,
  Entity,
  JoinColumn,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';

import { LoyaltySystem } from '../models';
import { ClientEntity } from '../client/client.entity';
import { IsArray, IsNotEmpty, IsNumber } from 'class-validator';

@Entity('loyalty-system')
export class LoyaltySystemEntity extends BaseEntity implements LoyaltySystem {
  @ApiProperty({ readOnly: true })
  @PrimaryGeneratedColumn({ type: 'int8' })
  id: number;

  @OneToOne(() => ClientEntity, (client) => client.loyaltySystem)
  @JoinColumn({ name: 'clientId' })
  client: ClientEntity;

  @ApiProperty()
  @IsNotEmpty()
  @IsNumber()
  @Column({ type: 'int8' })
  clientId: number;

  @ApiProperty()
  @IsArray()
  @IsNotEmpty()
  @Column({ type: 'simple-array' })
  usedCoupons: string[];

  @ApiProperty()
  @IsNotEmpty()
  @Column({ type: 'int' })
  bonuses: number;
}
