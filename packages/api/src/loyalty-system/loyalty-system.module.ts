import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { LoyaltySystemEntity } from 'src/loyalty-system/loyalty-system.entity';
import { LoyaltySystemController } from './loyalty-system.controller';
import { LoyaltySystemService } from './loyalty-system.service';

@Module({
  imports: [TypeOrmModule.forFeature([LoyaltySystemEntity])],
  controllers: [LoyaltySystemController],
  providers: [LoyaltySystemService],
})
export class LoyaltySystemModule {}
